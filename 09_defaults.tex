\section{Defaults}
Defaults make use of non-monotonic reasoning.
This means that for an entailment relation $\models$ the fact $\mathrm{KB}\models \alpha$ implies
$\mathrm{KB}^* \models \alpha$ for every $\mathrm{KB}^* \supseteq \mathrm{KB}$.
However this does not hold for entailment relations which are based on additional assumptions.
E.g. in closed-world reasoning (see below) and $\mathrm{KB} = (\neg \beta \lor \alpha)$ we have $\mathrm{KB} \models_C \alpha$.
This entailment is reverted when $\alpha$ is explicitly added to $\mathrm{KB}$.

\begin{itemize}
  \item \textbf{Closed-World-Assumption}:
    Whatever is not represented by KB does not hold.
    Define $\mathrm{Negs} = \{\neg p \mid \text{$p$ is atomic and $\mathrm{KB}\not\models p$}\}$
    We get a new entailment $KB \models_C \alpha \iff KB\cup \mathrm{Negs} \models \alpha$.
    However, KB is not necessarily consistent, e.g. for $\mathrm{KB} = \{\alpha\lor\beta\}$, but ok for horn-clauses.

    Atoms here can only contain constants but no qualified variables.
    Hence the resulting entailment relation $\models_C$ is only complete under domain closure.

  \item \textbf{General-Closed-World-Assumption (GCWA)}:
    Similar to the previous case but add only negative atoms which are ``ok'':
      \begin{multline*}
        \mathrm{Negs} = \{\neg p \mid \text{if $c = p\lor q_1\lor\ldots \lor q_n \in \mathrm{KB}$, $q_i$ atomic, and if} \\
        \text{$\mathrm{KB}\models c$ then $\mathrm{KB} \models q_1\lor\ldots\lor q_n$}\} \\
      \end{multline*}
    The general closed world assumption (GCWA) \textit{preserves consistency}.

  \item \textbf{Minimizing Abnormalities}:
    Introduce an abnormality predicate $\mathrm{Ab}$. Then $\mathrm{KB} \models_m \alpha$ if $\alpha$ is entailed by all knowledge
    bases with minimal abnormalities.
    Here no inconsistencies can arise from a consistent knowledge base.
    Additionally, it is stronger than GCWA.

  \item \textbf{Default Logic}:
    Use triplets $\langle \alpha,\beta,\gamma\rangle$ corresponding to default rules: if $\alpha$ and $\beta$ is consistent with KB
    then infer $\gamma$.
    The Knowledge Base is then given as $\mathrm{KB} = \langle F,D\rangle$ where $F$ is a set of facts (sentences in FOL) and $D$
    is a set of default rules.
    The special case of normal rules $\langle\alpha,\beta,\beta\rangle$ is written as $\langle \alpha\Rightarrow \beta\rangle$.
    The semantics is then given w.r.t. an extension of $\langle F,D\rangle$ such that
    \[\varphi \in E \iff F\cup \Delta \models \varphi \quad \text{ with } \Delta = \{\gamma\mid
    \langle\alpha,\beta,\gamma\rangle\in D, \alpha\in E, \neg\beta\not\in E\}\]

    However this definition means that $E$ can contain sentences which are ``orthogonal'' to the knowledge base.
    Hence we give the following definition (a computation can make use of fixed-point semantics).
    For $\mathrm{KB}=\langle F,D\rangle$, let $\Gamma(S)$ be the least set containing $F$ and such that
    \[\text{if $\langle\alpha,\beta,\gamma\rangle \in D, \alpha \in \Gamma(S), \underbrace{\neg\beta\not\in S}_{\mathclap{\text{Don't know why not $\Gamma(S)$}}}$ then $\gamma\in\Gamma(S)$}\]

  \item \textbf{Autoepistemic Logic}:
    Introduce a special belief symbol $\mathbf{B}$.
    \highlightbox{Properties for sets of implicit beliefs $E$}{
      \begin{enumerate}
        \item \textbf{Entailment}: if $E\models \alpha$ then $\alpha\in E$
        \item \textbf{Positive Introspection}: if $\alpha\in E$ then $\textbf{B}\alpha \in E$
        \item \textbf{Positive Introspection}: if $\alpha\not\in E$ then $\neg\textbf{B}\alpha \in E$
      \end{enumerate}
      Any such sets of sentences is called \textbf{stable}.
    }

    For propositional knowledge bases where every $\mathbf{B}$ operator only operates on \textit{objective formulas} (formulas without a $\mathbf{B}$ operator)
    such stable expansions can easily be computed by a brute-force approach:

    \highlightbox{Computation of stable Expansions}{
      \begin{enumerate}
        \item Nondeterministically replace each $\mathbf{B}\alpha_1,\mathbf{B}\alpha_2,\ldots$
          a truth value. Call the result $\mathrm{KB}^°$

        \item Check for each $\alpha_i$:
          \begin{enumerate}
            \item if $\mathbf B\alpha_i$ was assigned true then $\mathrm{KB}^°\models \alpha$
            \item if $\mathbf B\alpha_i$ was assigned false then $\mathrm{KB}^° \not\models \alpha$
          \end{enumerate}
        \item If yes to all, then $\mathrm{KB}^°$ determines a stable expansion
      \end{enumerate}
    }
\end{itemize}
